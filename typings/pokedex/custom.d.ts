interface Env {
  prod: boolean;
  beta: boolean;
  dev: boolean;
}

declare interface Window {
  adsbygoogle: any[];
  env: Env;
}

declare interface Navigator {
  standalone: boolean;
  serviceWorker: any;
}

declare var adsbygoogle: any[];
declare var ga: any;

// Needed to make relative paths work
declare var module: { id: any };