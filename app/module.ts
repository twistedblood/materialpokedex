import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './components/app';
import { AdComponent } from './components/ad';
import { InputDebounceComponent } from './components/debounce';
import { Api } from './components/api';
import { PokedexEntryComponent } from './components/pokedex-entry';

import { FilterPipe } from './pipes/filter';
import { JustAFewPipe } from './pipes/justafew';
import { MeasurementPipe } from './pipes/measurement';

@NgModule({
  imports: [ BrowserModule, HttpModule, FormsModule ],
  declarations: [AppComponent, PokedexEntryComponent,
    AdComponent, InputDebounceComponent,
    FilterPipe, JustAFewPipe, MeasurementPipe],
  providers: [ Api ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

if (window.env.prod) enableProdMode();
if (window.env.beta) console.log('BETA');
if (window.env.dev) console.log('DEV');