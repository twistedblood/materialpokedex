import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'measure'
})
export class MeasurementPipe implements PipeTransform {
  public transform(value: number, type: string, imperial: boolean): string {
    switch (type.toLowerCase()) {
      case 'weight':
        return this.Weight(value, imperial);
      default:
        return this.Height(value, imperial);
    }
  }
  
  private Height(value: number, imperial: boolean): string {
    let str = '';
    if (imperial) {
      value *= 3.93701;
      let feet = Math.floor(value / 12);
      let inches = value % 12;
      str = feet + "' " + inches.toFixed(1) + '"';
    } else {
      value *= 100;
      if (value > 1000) {
        str = (value / 1000).toFixed(1) + ' m';
      } else {
        str = (value / 10).toFixed(0) + ' cm';
      }
    }
    return str;
  }
  
  private Weight(value: number, imperial: boolean): string {
    let str = '';
    if (imperial) {
      value *= 0.220462;
      str = value.toFixed(1) + ' lbs';
    } else {
      str = (value / 10) + ' kg';
    }
    return str;
  }
}