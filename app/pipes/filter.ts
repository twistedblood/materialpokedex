import { Pipe, PipeTransform } from '@angular/core';

import { MinPokemon } from '../models/base';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
  public transform(value: MinPokemon[], term: string, ver: number, lang: number): MinPokemon[] {
    if (!term && !ver) return value.slice(1);
    let id = parseInt(term);
    if (ver == 0) ver = 10000;
    var results: MinPokemon[];
    if (!isNaN(id)) {
      results = value.filter(p => p.earliest_version <= ver && p.id == id);
    } else {
      term = term.toUpperCase();
      results = value.filter(p => p.id && p.earliest_version <= ver && p.names[lang].toUpperCase().indexOf(term) != -1);
    }
    return results;
  }
}