import { platformBrowser }    from '@angular/platform-browser';
import { AppModuleNgFactory } from '../build/app/module.ngfactory';
platformBrowser().bootstrapModuleFactory(AppModuleNgFactory)
  .catch((err: any) => console.error(err));