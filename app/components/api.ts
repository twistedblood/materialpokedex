import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import '../rxjs-operators';

import { Base } from '../models/base';
import { ExtendedData } from '../models/extended';
import { Card, CardSet } from '../models/card';

const TCG_API = 'https://api.pokemontcg.io/v1/';

@Injectable()
export class Api {

  constructor(private http: Http) { }

  public LoadBaseData(): Observable<Base> {
    return this.http.get('data/base.json').map(res => new Base(res.json()));
  }

  public LoadExtendedData(id: number): Observable<ExtendedData> {
    return this.http.get('data/pokemon/' + id + '.json').map(res => new ExtendedData(res.json()));
  }

  public LoadTCGSetData(): Observable<{ sets: CardSet[] }> {
    return this.http.get(TCG_API + 'sets').map(res => res.json());
  }

  public LoadTCGData(id: number): Observable<{ cards: Card[] }> {
    return this.http.get(TCG_API + 'cards?nationalPokedexNumber=' + id).map(res => res.json());
  }
}