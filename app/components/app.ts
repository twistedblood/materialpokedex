import { Component, AfterViewInit } from '@angular/core';
import { Base, MinPokemon, Language, Version } from '../models/base';
import { ExtendedData } from '../models/extended';
import { Card, CardSet } from '../models/card';
import { Api } from './api';

import '../rxjs-operators';

@Component({
  moduleId: module.id,
  selector: 'app',
  templateUrl: '../views/app.html'
})
export class AppComponent implements AfterViewInit {
  base: Base = new Base(null);
  data: ExtendedData[] = [];
  search: string = '';
  SelectedPokemon: MinPokemon = null;
  SelectedLang: number = 9;
  SelectedVer: number = 0;
  MovesLearnedType: number = 1;
  
  ShowPanel: boolean = false;
  ShowTCGPanel: boolean = false;
  CardGallery: Flickity[] = [];
  CardSets: CardSet[] = [];
  SelectedTCGCard: number = 0;
  
  LoadingSugimori: boolean = false;
  working = true;
  win: Window = window;
  Math: Math = Math;
  ads = false;
  scrollPos = 0;
  wasUpdateHash = false;
  
  constructor(private api: Api) {
    this.api.LoadTCGSetData().subscribe(data => {
      this.CardSets = data.sets;
    });

    this.api.LoadBaseData().subscribe(base => {
      if (window.env.dev) console.log(base);
      this.working = false;
      this.base = base;
      this.ParseHash();
    });
    
    this.preloadImages([
      'img/pokeball.png',
      'img/pokeball-dark.png',
      'img/sprites.png',
      'img/forms.png',
      'img/type-badges.png'
    ]);

    this.ads = window.localStorage.getItem('ShowAds') == '1';

    this.UpdateTitle();
  }

  ngAfterViewInit() {
    window.addEventListener('popstate', e => {
      if (!this.wasUpdateHash) {
        this.ClosePanel();
      }
      this.wasUpdateHash = false;
    });
  }

  ParseHash() {
    let parts: string[] = [];
    if (window.location.hash) {
      let hash = window.location.hash.substring(1);
      while (hash.endsWith('/')) hash = hash.substring(0, hash.length - 1);
      parts = hash.split('/');
      parts = parts.filter(p => !!p);
      if (parts.length) {
        let id = parseInt(parts[0]);
        if (!this.SelectedPokemon && id != null && this.base && this.base.pokemon[id]) {
          // Don't call SelectPokemon(), don't need everything it does
          this.SelectedPokemon = this.base.pokemon[id];
          this.getExtraData(id);
          this.ShowPanel = true;
          this.LoadingSugimori = true;
          if (parts.length >= 2) {
            // Don't call ShowTCGPanel, don't need everything it does
            this.ShowTCGPanel = true;
            if (window.innerWidth >= 768) $('.info-panel').scrollTop(0);
            this.CreateGallery();
          } else {
            this.ShowTCGPanel = false;
          }
          this.LoadCards(this.SelectedPokemon, parts[2]);// no need to check if parts[2] exists, it's null checked
          // UI needs to refresh first to create the
          // element we want to scroll into view
          this.scrollPos = id - 1;
          setTimeout(() => {
            $('#pokemon-entry-' + this.SelectedPokemon.id)[0].scrollIntoView(true);
            document.body.scrollTop = 0;
          }, 10);
        } else {
          this.SelectedPokemon = null;
          this.UpdateHash();
        }
      }
    } else {
      this.ShowTCGPanel = false;
      this.ShowPanel = false;
      this.SelectedPokemon = null;
    }
  }
  
  preloadImages(images: string[]) {
    if (images && images.length) {
      for (let i = 0; i < images.length; i++) {
        if (images[i]) {
          let img = new Image();
          img.src = images[i];
        }
      }
    }
  }
  
  onVersionChange(ver: number) {
    this.SelectedVer = ver;
    if (this.SelectedPokemon && ver != 0 && this.SelectedPokemon.earliest_version > ver) {
      this.SelectedPokemon = null;
      this.UpdateTitle();
      this.UpdateHash();
      this.scrollPos = 0;
      $('.pokemon-column').scrollTop(0);
    }
    if (!this.SelectedPokemon || !this.data[this.SelectedPokemon.id].AnyMovesLearnedByMethod(this.base.versions[this.SelectedVer].version_group_id, this.MovesLearnedType)) {
      this.MovesLearnedType = 1;
    }
  }

  onLanguageChange(lid: number) {
    this.SelectedLang = lid;
    this.UpdateTitle();
  }
  
  SelectPokemon(id: number) {
    let p = this.base.pokemon[id];
    this.ShowTCGPanel = false;
    if (window.env.dev) console.log('SelectPokemon: ', p);
    if (this.SelectedPokemon == p) return;
    if (this.CardGallery) {
      this.CardGallery.forEach(cg => cg.destroy());
      this.CardGallery.length = 0;
    }
    this.SelectedPokemon = p;
    this.getExtraData(p.id, true);
    this.LoadCards(p);
    this.LoadingSugimori = true;
    if (this.data[this.SelectedPokemon.id] && !this.data[this.SelectedPokemon.id].AnyMovesLearnedByMethod(this.base.versions[this.SelectedVer].version_group_id, this.MovesLearnedType)) {
      this.MovesLearnedType = 1;
    }
    this.CollapseNavbar();
    this.UpdateHash();
    this.UpdateTitle();
    if (!window.env.dev) {
      ga('set', 'page', window.location.pathname + '#' + p.id);
      ga('send', 'pageview');
    }
  }

  Search_onEnter() {
    if (window.innerWidth < 768) {
      this.ShowTCGPanel = false;
      this.ClosePanel();
      this.CollapseNavbar();
    }
  }

  onSearch(term: string) {
    this.search = term;
    if (window.innerWidth < 768) return;
    if (term || !this.SelectedPokemon) {
      $('.pokemon-column').scrollTop(0);
    } else if (this.SelectedPokemon) {
      this.scrollPos = this.SelectedPokemon.id;
      setTimeout(() => {
        $('#pokemon-entry-' + this.SelectedPokemon.id)[0].scrollIntoView(true);
      }, 10);
    }  
  }

  CollapseNavbar() {
    $('.navbar-collapse').collapse('hide');
    $('.info-panel').scrollTop(0);
  }

  getExtraData(id: number, open: boolean = false) {
    if (!this.data[id]) {
      this.working = true;
      this.api.LoadExtendedData(id).subscribe(ext => {
        this.data[id] = ext;
        if (window.env.dev) console.log(ext);
        if (open) this.ShowPanel = true;
        this.working = false;
      });
    } else if (open) {
      this.ShowPanel = true;
    }
  }

  LoadCards(p: MinPokemon, id: string = null) {
    if (this.CardSets && (!p.cards || p.cards.length == 0)) {
      this.api.LoadTCGData(p.id).subscribe(data => {
        let cards = data.cards;
        cards.sort((a, b) => {
          let setA = this.CardSets.findIndex(s => s.code == a.setCode);
          let setB = this.CardSets.findIndex(s => s.code == b.setCode);
          if (setA == setB) {
            return parseInt(a.number) - parseInt(b.number);
          } else {
            return setA - setB;
          }
        });
        p.cards = cards;
        if (id) {
          let index = cards.findIndex(c => c.id == id);
          if (index != -1 && index < cards.length) {
            this.SelectedTCGCard = index;
          } else {
            this.SelectedTCGCard = 0;
          }
        }
        this.UpdateHash();
        setTimeout(() => this.CreateGallery(), 1);
      });
    }
  }
  
  ClosePanel() {
    if (this.ShowTCGPanel) {
      this.SelectedTCGCard = 0;
      this.ShowTCGPanel = false;
      this.UpdateHash();
    } else {
      this.ShowPanel = false;
      // closing the panel should take 300ms
      setTimeout(() => {
        this.SelectedPokemon = null;
        this.UpdateHash();
        this.UpdateTitle();
      }, 350);
    }
  }
  
  ToggleAds() {
    this.ads = !this.ads;
    window.localStorage.setItem('ShowAds', this.ads ? '1' : '0');
  }

  UpdateTitle() {
    let title = 'Material Pokedex';
    if (window.env.beta) title += ' Beta';
    if (window.env.dev) title += ' Dev';
    if (this.SelectedPokemon) {
      title += ' - #' + this.SelectedPokemon.id + ' ' + this.SelectedPokemon.getName(this.SelectedLang);
    }
    if (window.env.dev) console.log('UpdateTitle: ' + title);
    document.title = title;
  }

  UpdateHash() {
    let h = '#';
    if (this.SelectedPokemon) {
      h += this.SelectedPokemon.id;
      if (this.ShowTCGPanel && this.SelectedPokemon.cards[this.SelectedTCGCard]) {
        h += '/cards/' + this.SelectedPokemon.cards[this.SelectedTCGCard].id;
      } else if (this.ShowTCGPanel) {
        h += '/cards';
      }
    } else {
      h = '';
    }
    if (window.env.dev) console.log('UpdateHash: ' + h);
    this.wasUpdateHash = true;
    window.location.hash = h;
    setTimeout(() => this.wasUpdateHash = false, 400);
  }

  ColScroll(event: Event) {
    let pos = $(event.target).scrollTop();
    this.scrollPos = Math.floor(pos / 132);
  }

  OpenTCGPanel() {
    this.SelectedTCGCard = 0;
    this.CardGallery.forEach(cg => cg.select(this.SelectedTCGCard));
    this.ShowTCGPanel = true;
    if (window.innerWidth >= 768) $('.info-panel').scrollTop(0);
    this.CreateGallery();
    this.UpdateHash();
    if (this.SelectedPokemon.cards[this.SelectedTCGCard]) {
      if (!window.env.dev) {
        ga('set', 'page', window.location.pathname + '#' + this.SelectedPokemon.id + '/cards/' + this.SelectedPokemon.cards[this.SelectedTCGCard].id);
        ga('send', 'pageview');
      }
    }
    setTimeout(() => {
      this.CardGallery.forEach(cg => cg.resize());
    }, 1);
  }

  CreateGallery() {
    if (this.CardGallery.length == 0 && $('.gallery').length) {
      this.CardGallery.push(new Flickity('.gallery-xs', { prevNextButtons: false, pageDots: false, imagesLoaded: true }));
      this.CardGallery.push(new Flickity('.gallery-sm', { pageDots: false, imagesLoaded: true }));
      this.CardGallery.forEach(cg => {
        cg.select(this.SelectedTCGCard);
        cg.on('select', () => {
          this.SelectCard(cg.selectedIndex);
        });
      });
    }
  }

  GetCardSet(code: string): CardSet {
    let sets = this.CardSets.filter(s => s.code == code);
    if (sets.length > 0) return sets[0];
    return null;
  }

  SelectCard(i: number) {
    if (this.SelectedTCGCard == i) return;
    
    this.SelectedTCGCard = i;
    this.UpdateHash();
    if (window.env.dev) console.log('SelectCard: ', i, this.SelectedPokemon.cards[i]);
    if (!window.env.dev) {
      ga('set', 'page', window.location.pathname + '#' + this.SelectedPokemon.id + '/cards/' + this.SelectedPokemon.cards[i].id);
      ga('send', 'pageview');
    }
  }
}