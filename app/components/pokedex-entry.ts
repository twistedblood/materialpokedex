import { Component, Input } from '@angular/core';

import { ExtendedData } from '../models/extended';

@Component({
  moduleId: module.id,
  selector: 'entry',
  templateUrl: '../views/pokedex-entry.html'
})
export class PokedexEntryComponent {
  @Input() pokemon: ExtendedData;
  @Input() language: number = 9;
  Math: Math = Math;
  
  constructor() {}
}