import { Component, AfterViewInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'ad',
  templateUrl: '../views/ad.html'
})
export class AdComponent implements AfterViewInit {
  dev: boolean = window.env.dev;

  ngAfterViewInit() {
    if (window.env.dev) return;
    try {
      (adsbygoogle = window.adsbygoogle || []).push({});
    } catch (e) {}
  }
}