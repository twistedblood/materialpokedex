import { Component, Input, Output, ElementRef, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Component({
  moduleId: module.id,
  selector: 'input-debounce',
  templateUrl: '../views/debounce.html'
})
export class InputDebounceComponent {
  @Input() placeholder: string;
  @Input() delay: number = 300;
  @Output() value: EventEmitter<string> = new EventEmitter<string>();
  @Output() enter: EventEmitter<boolean> = new EventEmitter<boolean>();

  public inputValue: string;

  constructor(private elementRef: ElementRef) {
    const eventStream = Observable.fromEvent(elementRef.nativeElement, 'keyup')
      .map(() => this.inputValue)
      .debounceTime(this.delay)
      .distinctUntilChanged();

    eventStream.subscribe(input => this.value.emit(input));
  }

  onClear() {
    this.inputValue = '';
    this.value.emit('');
  }

  onEnter() {
    this.enter.emit(true);
    this.value.emit(this.inputValue);
  }
}