export interface Card {
  id: string;
  name: string;
  nationalPokedexNumber: number;
  imageUrl: string;
  subtype: string;
  supertype: string;
  hp: string;
  retreatCost: string[];
  number: string;
  artist: string;
  rarity: string;
  series: string;
  set: string;
  setCode: string;
  types: string[];
  attacks: CardAttack[];
  weaknesses: CardWeakness[];
}

export interface CardAttack {
  cost: string[];
  name: string;
  text: string;
  damage: string;
  convertedEnergyCost: number;
}

export interface CardWeakness {
  type: string;
  value: string;
}

export interface CardSet {
  code: string;
  name: string;
  series: string;
  totalCards: number;
  standardLegal: boolean;
  releaseDate: string;
}