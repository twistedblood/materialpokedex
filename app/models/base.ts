import { Card } from './card';

export class Base {
  public pokemon: MinPokemon[] = [];
  public languages: Language[] = [];
  public versions: Version[] = [];

  constructor(obj: any) {
    Object.assign(this, obj);
    this.pokemon = this.pokemon.map(p => new MinPokemon(p));
    this.languages = this.languages.map(l => new Language(l));
    this.versions = this.versions.map(v => new Version(v));
    this.versions.unshift(new Version({ id: 0, names: [null, null, null, null, null, null, null, null, null, 'All'], version_group_id: 0 }));
  }
}

export class MinPokemon {
  id: number = 0;
  earliest_version: number;
  names: string[];
  types: number[];
  cards: Card[] = [];

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
} 

export class Language {
  public id: number = 0;
  public names: string[] = [];
  public order: number = 0;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }
  
  getName(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
}

export class Version {
  public id: number = 0;
  public names: string[] = [];
  public version_group_id: number = 0;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
}