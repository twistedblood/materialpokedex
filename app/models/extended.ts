export class ExtendedData {
  public genus: string[] = [];
  public flavor_texts: string[][] = [];
  public habitat_names: string[] = [];
  public egg_group_names: string[][] = [];
  public growth_rate_names: string[] = [];
  public abilities: Ability[] = [];
  public dex_numbers: DexNumber[] = [];
  public moves: Move[][][] = [];
  public shape: Shape;
  public evolution_chain: EvolutionLink[] = [];
  public earliest_version: number;
  public forms: Form[] = [];
  public id: number;
  public height: number;
  public weight: number;
  public base_experience: number;
  public is_default: boolean;
  public generation_id: number;
  public evolves_from_species_id: number;
  public evolution_chain_id: number;
  public color_id: number;
  public gender_rate: number;
  public capture_rate: number;
  public base_happiness: number;
  public is_baby: boolean;
  public hatch_counter: number;
  public base_hp: number;
  public base_attack: number;
  public base_defense: number;
  public base_sp_attack: number;
  public base_effort: number[];
  public base_sp_defense: number;
  public base_speed: number;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
    
    this.abilities = this.abilities.map(a => new Ability(a));
    this.dex_numbers = this.dex_numbers.map(d => new DexNumber(d));
    this.moves = this.moves.map(v => v.map(meth => meth.map(m => new Move(m))));
    this.shape = new Shape(this.shape);
    this.evolution_chain = this.evolution_chain.map(e => new EvolutionLink(e));
    this.forms = this.forms.map(f => new Form(f));
  }

  getFlavorText(ver: number, lang: number): [number, string] {
    if (this.id == 0 || this.flavor_texts.length == 0 || isNaN(ver)) return [0, ''];
    while (!this.flavor_texts[ver]) {
      ver++;
    }
    if (!lang || !this.flavor_texts[ver][lang]) lang = 9;
    return [ ver, this.flavor_texts[ver][lang] ];
  }

  getAbilities(): Ability[] {
    return this.abilities.filter(a => !!a && !!a.id);
  }

  getGenus(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.genus[lang]) lang = 9;
    return this.genus[lang];
  }

  getHabitatName(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.habitat_names[lang]) lang = 9;
    return this.habitat_names[lang];
  }

  getCaptureRate(): number {
    if (this.id == 0) return 0;
    let percent = ((this.base_hp * this.capture_rate) / (3 * this.base_hp)) / 255;
    percent = Math.floor(percent * 10000) / 100;
    return percent;
  }

  getEggGroupNames(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.egg_group_names[0][lang]) lang = 9;
    return this.egg_group_names.map(egg => egg[lang]).join(', ');
  }

  getGrowthRateName(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.growth_rate_names[lang]) lang = 9;
    return this.growth_rate_names[lang];
  }

  getEffortValue(): string {
    if (this.id == 0) return '';
    var str = '';
    str += this.base_effort[1] + ' ';
    switch (this.base_effort[0]) {
      case 1:
        str += 'HP';
        break;
      case 2:
        str += 'ATK';
        break;
      case 3:
        str += 'DEF';
        break;
      case 4:
        str += 'SA';
        break;
      case 5:
        str += 'SD';
        break;
      case 6:
        str += 'SPD';
        break;
      case 7:
        str += 'ACC';
        break;
      case 8:
        str += 'EVA';
        break;
    }
    return str;
  }

  hasForms(): boolean {
    return this.forms.length > 1;
  }

  AnyMovesLearnedByMethod(ver: number, method: number): boolean {
    if (this.id == 0 || this.moves.length == 0 || isNaN(ver)) return false;
    while (!this.moves[ver].length) ver++;
    return this.moves[ver][method] && this.moves[ver][method].length != 0;
  }

  getMoves(ver: number, method: number): Move[] {
    if (this.id == 0 || this.moves.length == 0 || isNaN(ver)) return [];
    while (!this.moves[ver].length) ver++;
    return this.moves[ver][method];
  }
}

export class Ability {
  public id: number;
  public names: string[] = [];
  public flavor_texts: string[][] = [];
  public hidden: boolean;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (this.id == 0) return '';
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }

  getFlavorText(ver: number, lang: number): string {
    if (this.id == 0 || this.flavor_texts.length == 0 || isNaN(ver)) return '';
    while (!this.flavor_texts[ver] || !this.flavor_texts[ver].length) {
      ver++;
    }
    if (!lang || !this.flavor_texts[ver][lang]) lang = 9;
    return this.flavor_texts[ver][lang];
  }
}

export class DexNumber {
  public names: string[] = [];
  public number: number;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
}

export class Move {
  public type_id: number;
  public names: string[] = [];
  public level: number;
  public machine: string;
  public order: number;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
}

export class Shape {
  public names: string[] = [];
  public awesome_names: string[] = [];

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }

  getAwesomeName(lang: number): string {
    if (lang == undefined || lang == null || !this.awesome_names[lang]) lang = 9;
    return this.awesome_names[lang];
  }
}

export class EvolutionLink {
  public id_from: number;
  public names_from: string[] = [];
  public evo: Evolution;
  public id_to: number;
  public names_to: string[] = [];

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
    this.evo = new Evolution(this.evo);
  }
}

export class Evolution {
  public evolved_species_id: number;
  public evolution_trigger_id: number;
  public evolution_trigger_name: string[] = [];
  public trigger_item: string[] = [];
  public minimum_level: number;
  public gender_id: number;
  public location: string[] = [];
  public held_item: string[] = [];
  public time_of_day: string;
  public known_move: string[] = [];
  public known_move_type: string[] = [];
  public minimum_happiness: number;
  public minimum_beauty: number;
  public minimum_affection: number;
  public relative_physical_stats: number;
  public party_species: string[] = [];
  public party_type: string[] = [];
  public trade_species: string[] = [];
  public needs_overworld_rain: boolean;
  public turn_upside_down: boolean;

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  public generateExplaination(lang: number): string {
    if (!lang
      || (this.held_item && !this.held_item[lang])
      || (this.location && !this.location[lang])
      || (this.known_move && !this.known_move[lang])) lang = 9;
    let output: string[] = [];
    
    switch (this.evolution_trigger_id) {
      case 3:
        output.push('Use ' + this.trigger_item[lang]);
        break;
      case 4:
        return 'Shedinja will consume a Poke Ball and appear in a free party slot';
      default:
        output.push(this.evolution_trigger_name[lang]);
        break;
    }
    
    if (this.minimum_level) output.push('starting at level ' + this.minimum_level);
    
    if (this.held_item) output.push('while holding ' + this.held_item[lang]);
    
    if (this.party_species) output.push('with ' + this.party_species[lang] + ' in the party');
    
    if (this.time_of_day) output.push('during the ' + this.time_of_day);
    
    if (this.minimum_beauty) output.push('with at least ' + this.minimum_beauty + ' beauty');
    
    if (this.minimum_happiness) output.push('with at least ' + this.minimum_happiness + ' happiness');
    
    if (this.known_move) output.push('knowing ' + this.known_move[lang]);
    
    if (this.known_move_type) output.push('knowing a ' + this.known_move_type[lang] + '-type move');
    
    if (this.party_type) output.push('with a ' + this.party_type[lang] + '-type Pokemon in the party');
    
    if (this.trade_species) output.push('in exchange for ' + this.trade_species[lang]);
    
    if (this.minimum_affection) output.push('with at least ' + this.minimum_affection + ' affection in Pokemon-Amie');
    
    switch (this.relative_physical_stats) {
      case 1:
        output.push('when Attack > Defense');
        break;
      case -1:
        output.push('when Attack < Defense');
        break;
      case 0:
        output.push('when Attack = Defense');
        break;
    }
    
    if (this.location) output.push('around ' + this.location[lang]);
    
    if (this.needs_overworld_rain) output.push('while it is raining outside of battle');
    
    if (this.turn_upside_down) output.push('with the 3DS turned upside-down');
    
    if (this.gender_id) output.push(this.gender_id == 1 ? 'female only' : 'male only');
    
    return output.join(', ');
  }
}

export class Form {
  public id: number;
  public names: string[] = [];
  public types: number[] = [];

  constructor(obj: any) {
    if (!obj) return;
    Object.assign(this, obj);
  }

  getName(lang: number): string {
    if (lang == undefined || lang == null || !this.names[lang]) lang = 9;
    return this.names[lang];
  }
}