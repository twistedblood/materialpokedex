import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './module';

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch((err: any) => console.error(err));