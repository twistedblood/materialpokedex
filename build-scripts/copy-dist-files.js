var fs = require('fs');
var ncp = require('ncp').ncp;

var target = 'build/';

ncp('aot.html', target + 'index.html', function (err) {
  if (err) {
    console.log(err);
    process.exit(1);
  } else {
    console.log('Moved aot.html -> index.html');
  }
});