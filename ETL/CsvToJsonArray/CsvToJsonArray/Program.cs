using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CsvToJsonArray {
  class Program {
    static void Main(string[] args) {
      String inDir = args[0];
			String outDir = args[1];

			#region Base.json
			Base b = new Base();
			String[][] pokemon = ReadCSV(inDir + "pokemon.csv");
			foreach (String[] line in pokemon) {
				if (line[7] != "1") continue;
				MinPokemon minp = new MinPokemon();
				int id = int.Parse(line[0]);
				minp.id = id;
				while (b.pokemon.Count <= id) b.pokemon.Add(null);
				b.pokemon[id] = minp;
			}

			#region Languages
			String[][] languages = ReadCSV(inDir + "languages.csv").Where(line => line[4] == "1").ToArray();
			int langCount = languages.Select(l => int.Parse(l[0])).Max();
			while (b.languages.Count <= langCount) b.languages.Add(null);
			String[][] language_names = ReadCSV(inDir + "language_names.csv");
			foreach (String[] line in languages) {
				Language lang = new Language();
				lang.id = int.Parse(line[0]);
				lang.order = int.Parse(line[5]);
				List<String[]> names = language_names.Where(l => l[0] == line[0]).ToList();
				List<String> langs = new List<String>();
				while (langs.Count < langCount) langs.Add(null);
				foreach (String[] name in names) {
					int lid = int.Parse(name[1]);
					langs[lid] = name[2];
				}
				lang.names = langs;
				b.languages[lang.id] = lang;
			}
			b.languages[11].names[9] = "Japanese Kanji";
			//b.languages = b.languages.Where(lang => lang != null).OrderBy(lang => lang.order).ToList();
			langCount = b.languages.Select(l => l != null ? l.id : -1).Max();
			#endregion

			#region Types
			String[][] pokemon_types = ReadCSV(inDir + "pokemon_types.csv").ToArray();
			foreach (String[] line in pokemon_types) {
				int pid = int.Parse(line[0]);
				int tid = int.Parse(line[1]);
				if (pid < b.pokemon.Count) b.pokemon[pid].types.Add(tid);
			}
			#endregion

			#region Versions
			String[][] versions = ReadCSV(inDir + "versions.csv");
			String[][] version_names = ReadCSV(inDir + "version_names.csv");
			foreach (String[] line in versions) {
				Version v = new Version();
				v.id = int.Parse(line[0]);
				v.version_group_id = int.Parse(line[1]);
				while (v.names.Count <= langCount) v.names.Add(null);
				String[][] names = version_names.Where(n => n[0] == line[0]).ToArray();
				foreach (String[] n in names) {
					int lid = int.Parse(n[1]);
					v.names[lid] = n[2];
				}
				b.versions.Add(v);
			}
			int verCount = b.versions.Count;
			#endregion

			#region Pokemon Names
			String[][] pokemon_species_names = ReadCSV(inDir + "pokemon_species_names.csv");
			foreach (String[] line in pokemon_species_names) {
				int pid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (b.pokemon[pid].names.Count <= langCount) b.pokemon[pid].names.Add(null);
				b.pokemon[pid].names[lid] = line[2];
			}
			#endregion

			#endregion

			#region Individual Pokemon
			List<Pokemon> data = new List<Pokemon>();
			data.Add(null);
			while (data.Count < b.pokemon.Count) {
				Pokemon p = new Pokemon();
				p.flavor_texts.Add(null);
				while (p.flavor_texts.Count <= verCount) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					p.flavor_texts.Add(langs);
				}
				for (int i = 0; i <= langCount; i++) {
					p.genus.Add(null);
					p.habitat_names.Add(null);
					p.growth_rate_names.Add(null);
				}
				p.id = data.Count;
				data.Add(p);
			}

			#region genus
			foreach (String[] line in pokemon_species_names) {
				int pid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				data[pid].genus[lid] = line[3];
			}
			#endregion

			#region flavor_texts
			String[][] pokemon_species_flavor_text = ReadCSV(inDir + "pokemon_species_flavor_text.csv");
			foreach (String[] line in pokemon_species_flavor_text) {
				int pid = int.Parse(line[0]);
				int vid = int.Parse(line[1]);
				int lid = int.Parse(line[2]);
				data[pid].flavor_texts[vid][lid] = line[3];
			}
			foreach (Pokemon p in data) {
				if (p == null || p.flavor_texts == null) continue;
				for (int v = 0; v < p.flavor_texts.Count; v++) {
					if (p.flavor_texts[v] != null) {
						if (p.flavor_texts[v].All(ft => String.IsNullOrEmpty(ft))) {
							p.flavor_texts[v] = null;
						}
					}
				}
			}
			#endregion

			#region shapes
			String[][] pokemon_shape_prose = ReadCSV(inDir + "pokemon_shape_prose.csv");
			List<PokemonShape> shapes = new List<PokemonShape>();
			foreach (String[] line in pokemon_shape_prose) {
				int sid = int.Parse(line[0]);
				while (shapes.Count <= sid) {
					PokemonShape s = new PokemonShape();
					for (int i = 0; i <= langCount; i++) {
						s.awesome_names.Add(null);
						s.names.Add(null);
					}
					shapes.Add(s);
				}
				int lid = int.Parse(line[1]);
				shapes[sid].names[lid] = line[2];
				shapes[sid].awesome_names[lid] = line[3];
			}
			#endregion

			#region pokemon_species, habitat_names, growth_rates
			int x = 0;
			String[][] pokemon_species = ReadCSV(inDir + "pokemon_species.csv");
			String[][] pokemon_habitat_names = ReadCSV(inDir + "pokemon_habitat_names.csv");
			String[][] growth_rate_prose = ReadCSV(inDir + "growth_rate_prose.csv");
			foreach (String[] line in pokemon_species) {
				int pid = int.Parse(line[0]);
				data[pid].generation_id = int.Parse(line[2]);
				if (int.TryParse(line[3], out x)) {
					data[pid].evolves_from_species_id = x;
				}
				data[pid].evolution_chain_id = int.Parse(line[4]);
				data[pid].color_id = int.Parse(line[5]);
				data[pid].shape = shapes[int.Parse(line[6])];
				int hid = 0;
				if (int.TryParse(line[7], out hid)) {
					String[][] h_names = pokemon_habitat_names.Where(h => h[0] == line[7]).ToArray();
					foreach (String[] h in h_names) {
						int lid = int.Parse(h[1]);
						data[pid].habitat_names[lid] = h[2];
					}
				} else {
					data[pid].habitat_names[9] = "unknown";
				}
				data[pid].gender_rate = int.Parse(line[8]);
				data[pid].capture_rate = int.Parse(line[9]);
				data[pid].base_happiness = int.Parse(line[10]);
				data[pid].is_baby = line[11] == "1";
				data[pid].hatch_counter = int.Parse(line[12]);
				int gid = int.Parse(line[14]);
				String[][] g_names = growth_rate_prose.Where(g => g[0] == line[14]).ToArray();
				foreach (String[] g in g_names) {
					int lid = int.Parse(g[1]);
					data[pid].growth_rate_names[lid] = g[2];
				}
			}
			#endregion

			#region abilities
			String[][] ability_names = ReadCSV(inDir + "ability_names.csv");
			List<PokemonAbility> abilities = new List<PokemonAbility>();
			abilities.Add(null);
			foreach (String[] line in ability_names) {
				int aid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (abilities.Count <= aid) abilities.Add(new PokemonAbility());
				abilities[aid].id = aid;
				while (abilities[aid].names.Count <= langCount) abilities[aid].names.Add(null);
				abilities[aid].names[lid] = line[2];
			}
			String[][] ability_flavor_text = ReadCSV(inDir + "ability_flavor_text.csv");
			foreach (String[] line in ability_flavor_text) {
				int aid = int.Parse(line[0]);
				int vid = int.Parse(line[1]);
				int lid = int.Parse(line[2]);
				if (abilities[aid].flavor_texts.Count == 0) abilities[aid].flavor_texts.Add(null);
				while (abilities[aid].flavor_texts.Count <= verCount) abilities[aid].flavor_texts.Add(new List<String>());
				while (abilities[aid].flavor_texts[vid].Count <= langCount) abilities[aid].flavor_texts[vid].Add(null);
				abilities[aid].flavor_texts[vid][lid] = line[3];
			}
			String[][] pokemon_abilities = ReadCSV(inDir + "pokemon_abilities.csv");
			foreach (String[] line in pokemon_abilities) {
				int pid = int.Parse(line[0]);
				if (pid >= data.Count) continue;
				int aid = int.Parse(line[1]);
				bool hidden = line[2] == "1";
				int slot = int.Parse(line[3]);
				while (data[pid].abilities.Count <= slot) data[pid].abilities.Add(null);
				data[pid].abilities[slot] = abilities[aid].Clone(hidden);
			}
			#endregion

			#region egg_groups
			String[][] egg_group_prose = ReadCSV(inDir + "egg_group_prose.csv");
			List<List<String>> egg_groups = new List<List<String>>();
			egg_groups.Add(null);
			foreach (String[] line in egg_group_prose) {
				int egid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				if (egg_groups.Count <= egid) {
					List<String> langs = new List<String>();
					while (langs.Count < langCount) langs.Add(null);
					egg_groups.Add(langs);
				}
				egg_groups[egid][lid] = line[2];
			}
			String[][] pokemon_egg_groups = ReadCSV(inDir + "pokemon_egg_groups.csv");
			foreach (String[] line in pokemon_egg_groups) {
				int pid = int.Parse(line[0]);
				int egid = int.Parse(line[1]);
				data[pid].egg_group_names.Add(egg_groups[egid]);
			}
			#endregion

			#region pokedex names, numbers
			String[][] pokedex_prose = ReadCSV(inDir + "pokedex_prose.csv");
			List<List<String>> dex_names = new List<List<String>>();
			dex_names.Add(null);
			foreach (String[] line in pokedex_prose) {
				int did = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (dex_names.Count <= did) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					dex_names.Add(langs);
				}
				dex_names[did][lid] = line[2];
			}
			dex_names[11] = null;
			String[][] pokemon_dex_numbers = ReadCSV(inDir + "pokemon_dex_numbers.csv");
			foreach (String[] line in pokemon_dex_numbers) {
				int pid = int.Parse(line[0]);
				int did = int.Parse(line[1]);
				if (did == 11) continue;
				int num = int.Parse(line[2]);
				PokedexEntry entry = new PokedexEntry();
				entry.names = dex_names[did];
				entry.number = num;
				data[pid].dex_numbers.Add(entry);
			}
			#endregion

			#region earliest_version
			String[][] pokemon_game_indices = ReadCSV(inDir + "pokemon_game_indices.csv");
			foreach (String[] line in pokemon_game_indices) {
				int pid = int.Parse(line[0]);
				if (pid >= data.Count) continue;
				int vid = int.Parse(line[1]);
				b.pokemon[pid].earliest_version = data[pid].earliest_version = Math.Min(data[pid].earliest_version, vid);
			}
			#endregion

			#region height, weight, base_experience
			foreach (String[] line in pokemon) {
				int pid = int.Parse(line[0]);
				if (pid >= data.Count) continue;
				int h = int.Parse(line[3]);
				int w = int.Parse(line[4]);
				int exp = int.Parse(line[5]);
				data[pid].height = h;
				data[pid].weight = w;
				data[pid].base_experience = exp;
			}
			#endregion

			#region stats
			String[][] pokemon_stats = ReadCSV(inDir + "pokemon_stats.csv");
			foreach (String[] line in pokemon_stats) {
				int pid = int.Parse(line[0]);
				if (pid >= data.Count) continue;
				int sid = int.Parse(line[1]);
				int stat = int.Parse(line[2]);
				int effort = int.Parse(line[3]);
				switch (sid) {
					case 1: data[pid].base_hp = stat; break;
					case 2: data[pid].base_attack = stat; break;
					case 3: data[pid].base_defense = stat; break;
					case 4: data[pid].base_sp_attack = stat; break;
					case 5: data[pid].base_sp_defense = stat; break;
					case 6: data[pid].base_speed = stat; break;
				}
				if (effort != 0) {
					data[pid].base_effort = new List<int>() { sid, effort };
				}
			}
			#endregion

			#region locations
			String[][] location_names = ReadCSV(inDir + "location_names.csv");
			List<List<String>> LocationNames = new List<List<String>>();
			LocationNames.Add(null);
			foreach (String[] line in location_names) {
				int lid = int.Parse(line[0]);
				int lang = int.Parse(line[1]);
				while (LocationNames.Count <= lid) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					LocationNames.Add(langs);
				}
				LocationNames[lid][lang] = line[2];
			}
			#endregion

			#region types
			String[][] type_names = ReadCSV(inDir + "type_names.csv");
			List<List<String>> TypeNames = new List<List<String>>();
			TypeNames.Add(null);
			foreach (String[] line in type_names) {
				int tid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (TypeNames.Count <= tid) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					TypeNames.Add(langs);
				}
				TypeNames[tid][lid] = line[2];
			}
			#endregion

			#region items
			String[][] items = ReadCSV(inDir + "items.csv");
			String[][] item_names = ReadCSV(inDir + "item_names.csv");
			List<List<String>> ItemNames = new List<List<String>>();
			ItemNames.Add(null);
			foreach (String[] line in item_names) {
				int iid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (ItemNames.Count <= iid) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					ItemNames.Add(langs);
				}
				ItemNames[iid][lid] = line[2];
			}
			#endregion

			#region moves
			String[][] moves = ReadCSV(inDir + "moves.csv");
			List<int> MoveTypes = new List<int>();
			foreach (String[] line in moves) {
				int mid = int.Parse(line[0]);
				while (MoveTypes.Count <= mid) MoveTypes.Add(-1);
				MoveTypes[mid] = int.Parse(line[3]);
			}

			String[][] move_names = ReadCSV(inDir + "move_names.csv");
			List<List<String>> MoveNames = new List<List<String>>();
			MoveNames.Add(null);
			foreach (String[] line in move_names) {
				int mid = int.Parse(line[0]);
				int lid = int.Parse(line[1]);
				while (MoveNames.Count <= mid) {
					List<String> langs = new List<String>();
					while (langs.Count <= langCount) langs.Add(null);
					MoveNames.Add(langs);
				}
				MoveNames[mid][lid] = line[2];
			}

			String[][] machines = ReadCSV(inDir + "machines.csv");
			List<Dictionary<int, String>> MoveToMachine = new List<Dictionary<int, String>>();
			foreach (String[] line in machines) {
				int vid = int.Parse(line[1]);
				while (MoveToMachine.Count <= vid) MoveToMachine.Add(new Dictionary<int, String>());
				String name = items.FirstOrDefault(i => i[0] == line[2])[1].ToUpper();
				MoveToMachine[vid].Add(int.Parse(line[3]), name);
			}

			String[][] pokemon_moves = ReadCSV(inDir + "pokemon_moves.csv");
			foreach (String[] line in pokemon_moves) {
				int pid = int.Parse(line[0]);
				if (pid >= data.Count) continue;
				int vid = int.Parse(line[1]);
				int mid = int.Parse(line[2]);
				int mmid = int.Parse(line[3]);
				if (mmid > 4) continue;
				int lvl = int.Parse(line[4]);
				int order = -1;
				int.TryParse(line[5], out order);
				while (data[pid].moves.Count <= vid) data[pid].moves.Add(new List<List<PokemonMove>>());
				while (data[pid].moves[vid].Count <= mmid) data[pid].moves[vid].Add(new List<PokemonMove>());
				PokemonMove move = new PokemonMove();
				move.type_id = MoveTypes[mid];
				move.level = lvl;
				if (order != -1) move.order = order;
				move.names = MoveNames[mid];
				if (mmid == 4) {
					move.machine = MoveToMachine[vid][mid];
				}
				data[pid].moves[vid][mmid].Add(move);
				data[pid].moves[vid][mmid].Sort((one, two) => {
					int diff = one.level - two.level;
					if (diff != 0) return diff;
					if (!String.IsNullOrEmpty(one.machine) && !String.IsNullOrEmpty(two.machine)) {
						return one.machine.CompareTo(two.machine);
					}
					if (one.order.HasValue && two.order.HasValue) {
						return one.order.Value - two.order.Value;
					}
					return one.names[9].CompareTo(two.names[9]);
				});
			}
			#endregion

			#region evolution
			String[][] evolution_trigger_prose = ReadCSV(inDir + "evolution_trigger_prose.csv");
			String[][] pokemon_evolution = ReadCSV(inDir + "pokemon_evolution.csv");
			foreach (Pokemon p in data) {
				if (p == null) continue;
				Pokemon root = p;
				while (root.evolves_from_species_id.HasValue) root = data[root.evolves_from_species_id.Value];
				List<int> evolves_to = data.Where(d => d != null && d.evolves_from_species_id.HasValue && d.evolves_from_species_id.Value == root.id).Select(d => d.id).ToList();
				List<EvolutionLink> links = pokemon_evolution.Where(line => evolves_to.Contains(int.Parse(line[1])))
					.Select(line => {
						EvolutionLink link = new EvolutionLink();
						link.id_from = root.id;
						link.names_from = b.pokemon[root.id].names;
						link.evo = new Evolution();
						link.evo.evolved_species_id = int.Parse(line[1]);
						while (link.evo.evolution_trigger_name.Count <= langCount) link.evo.evolution_trigger_name.Add(null);
						link.evo.evolution_trigger_id = int.Parse(line[2]);
						evolution_trigger_prose.Where(etp => etp[0] == line[2]).ToList().ForEach(etp => {
							int lid = int.Parse(etp[1]);
							link.evo.evolution_trigger_name[lid] = etp[2];
						});
						if (int.TryParse(line[3], out x)) link.evo.trigger_item = ItemNames[x];
						if (int.TryParse(line[4], out x)) link.evo.minimum_level = x;
						if (int.TryParse(line[5], out x)) link.evo.gender_id = x;
						if (int.TryParse(line[6], out x)) link.evo.location = LocationNames[x];
						if (int.TryParse(line[7], out x)) link.evo.held_item = ItemNames[x];
						if (!String.IsNullOrEmpty(line[8])) link.evo.time_of_day = line[8];
						if (int.TryParse(line[9], out x)) link.evo.known_move = MoveNames[x];
						if (int.TryParse(line[10], out x)) link.evo.known_move_type = TypeNames[x];
						if (int.TryParse(line[11], out x)) link.evo.minimum_happiness = x;
						if (int.TryParse(line[12], out x)) link.evo.minimum_beauty = x;
						if (int.TryParse(line[13], out x)) link.evo.minimum_affection = x;
						if (int.TryParse(line[14], out x)) link.evo.relative_physical_stats = x;
						if (int.TryParse(line[15], out x)) link.evo.party_species = b.pokemon[x].names;
						if (int.TryParse(line[16], out x)) link.evo.party_type = TypeNames[x];
						if (int.TryParse(line[17], out x)) link.evo.trade_species = b.pokemon[x].names;
						link.evo.needs_overworld_rain = line[18] == "1";
						link.evo.turn_upside_down = line[19] == "1";

						link.id_to = link.evo.evolved_species_id;
						link.names_to = b.pokemon[link.evo.evolved_species_id].names;
						return link;
					}).ToList();
				List<EvolutionLink> chain = new List<EvolutionLink>();

				while (links.Count != 0) {
					EvolutionLink l = links[0];
					links.RemoveAt(0);
					chain.Add(l);

					evolves_to = data.Where(d => d != null && d.evolves_from_species_id.HasValue && d.evolves_from_species_id.Value == l.id_to.Value).Select(d => d.id).ToList();
					links.AddRange(pokemon_evolution.Where(line => evolves_to.Contains(int.Parse(line[1])))
					.Select(line => {
						EvolutionLink link = new EvolutionLink();
						link.id_from = root.id;
						link.names_from = b.pokemon[root.id].names;
						link.evo = new Evolution();
						link.evo.evolved_species_id = int.Parse(line[1]);
						while (link.evo.evolution_trigger_name.Count <= langCount) link.evo.evolution_trigger_name.Add(null);
						link.evo.evolution_trigger_id = int.Parse(line[2]);
						evolution_trigger_prose.Where(etp => etp[0] == line[2]).ToList().ForEach(etp => {
							int lid = int.Parse(etp[1]);
							link.evo.evolution_trigger_name[lid] = etp[2];
						});
						if (int.TryParse(line[3], out x)) link.evo.trigger_item = ItemNames[x];
						if (int.TryParse(line[4], out x)) link.evo.minimum_level = x;
						if (int.TryParse(line[5], out x)) link.evo.gender_id = x;
						if (int.TryParse(line[6], out x)) link.evo.location = LocationNames[x];
						if (int.TryParse(line[7], out x)) link.evo.held_item = ItemNames[x];
						if (!String.IsNullOrEmpty(line[8])) link.evo.time_of_day = line[8];
						if (int.TryParse(line[9], out x)) link.evo.known_move = MoveNames[x];
						if (int.TryParse(line[10], out x)) link.evo.known_move_type = TypeNames[x];
						if (int.TryParse(line[11], out x)) link.evo.minimum_happiness = x;
						if (int.TryParse(line[12], out x)) link.evo.minimum_beauty = x;
						if (int.TryParse(line[13], out x)) link.evo.minimum_affection = x;
						if (int.TryParse(line[14], out x)) link.evo.relative_physical_stats = x;
						if (int.TryParse(line[15], out x)) link.evo.party_species = b.pokemon[x].names;
						if (int.TryParse(line[16], out x)) link.evo.party_type = TypeNames[x];
						if (int.TryParse(line[17], out x)) link.evo.trade_species = b.pokemon[x].names;
						link.evo.needs_overworld_rain = line[18] == "1";
						link.evo.turn_upside_down = line[19] == "1";

						link.id_to = link.evo.evolved_species_id;
						link.names_to = b.pokemon[link.evo.evolved_species_id].names;
						return link;
					}).ToList());
				}

				if (chain.Count == 0) {
					p.evolution_chain = new List<EvolutionLink>() { new EvolutionLink() { id_from = p.id, names_from = b.pokemon[p.id].names, evo = null, id_to = null, names_to = null } };
				} else {
					chain.Sort((one, two) => {
						if (data[one.id_from].is_baby && !data[two.id_from].is_baby) {
							return -1;
						} else if (data[two.id_from].is_baby && !data[one.id_from].is_baby) {
							return 1;
						}
						int diff = one.id_from - two.id_from;
						if (diff != 0) {
							return diff;
						} else {
							diff = one.id_to.Value - two.id_to.Value;
							if (diff != 0) {
								return diff;
							} else {
								return one.evo.evolution_trigger_id - two.evo.evolution_trigger_id;
							}
						}
					});
					p.evolution_chain = chain;
				}
			}
			#endregion

			#region forms
			String[][] pokemon_forms = ReadCSV(inDir + "pokemon_forms.csv");
			String[][] pokemon_form_names = ReadCSV(inDir + "pokemon_form_names.csv");
			foreach (String[] line in pokemon_forms) {
				PokemonForm form = new PokemonForm();
				form.id = int.Parse(line[0]);
				form.form_order = int.Parse(line[8]);
				form.types = PokemonForm.AuxTypes(form.id);
				while (form.names.Count <= langCount) form.names.Add(null);
				List<String[]> formnames = pokemon_form_names.Where(name => name[0] == line[0]).ToList();
				if (formnames.Count != 0) {
					formnames.ForEach(name => form.names[int.Parse(name[1])] = name[2]);
				} else {
					form.names = b.pokemon[form.id].names;
				}
				int pid = int.Parse(line[3]);
				if (pid >= data.Count) {
					pid = int.Parse(pokemon.First(p => p[0] == line[3])[2]);
				}
				data[pid].forms.Add(form);
			}
			#endregion

			#endregion

			File.WriteAllText(outDir + "base.json", JsonConvert.SerializeObject(b, Formatting.None));
			if (!Directory.Exists(outDir + "pokemon")) Directory.CreateDirectory(outDir + "pokemon");
			for (int i = 1; i < data.Count; i++) {
				File.WriteAllText(outDir + "pokemon/" + i + ".json", JsonConvert.SerializeObject(data[i], Formatting.None));
			}
		}

		static String[][] ReadCSV(String path) {
			TextFieldParser parser = new TextFieldParser(path);
			parser.HasFieldsEnclosedInQuotes = true;
			parser.SetDelimiters(",");
			parser.TrimWhiteSpace = true;
			parser.ReadFields();// remove header row
			List<String[]> lines = new List<String[]>();
			while (!parser.EndOfData) {
				String[] fields = parser.ReadFields();
				for (int i = 0; i < fields.Length; i++) {
					fields[i] = fields[i].Replace("\"", "\\\"").Replace("\f", " ").Replace("\r\n", " ").Replace("\n", " ");
					fields[i] = Regex.Replace(fields[i], @"\s{2,}", " ");
				}
				lines.Add(fields);
			}
			return lines.ToArray();
		}
  }
}
