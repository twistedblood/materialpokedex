using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvToJsonArray {
	public class Base {
		public List<MinPokemon> pokemon = new List<MinPokemon>();
		public List<Language> languages = new List<Language>();
		public List<Version> versions = new List<Version>();
	}

	public class MinPokemon {
		public int id;
		public int earliest_version = 23;
		public List<String> names = new List<String>();
		public List<int> types = new List<int>();
	}

	public class Language {
		public int id;
		public List<String> names = new List<String>();
		public int order;
	}

	public class Version {
		public int id;
		public List<String> names = new List<String>();
		public int version_group_id;
	}
}
