using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvToJsonArray {
	public class Pokemon {
		public List<String> genus = new List<String>();
		public List<List<String>> flavor_texts = new List<List<String>>();
		public List<String> habitat_names = new List<String>();
		public List<List<String>> egg_group_names = new List<List<String>>();
		public List<String> growth_rate_names = new List<String>();
		public List<PokemonAbility> abilities = new List<PokemonAbility>();
		public List<PokedexEntry> dex_numbers = new List<PokedexEntry>();
		public List<List<List<PokemonMove>>> moves = new List<List<List<PokemonMove>>>();
		public PokemonShape shape;
		public List<EvolutionLink> evolution_chain = null;
		public int earliest_version = 23;
		public List<PokemonForm> forms = new List<PokemonForm>();
		public int id;
		public int height;
		public int weight;
		public int base_experience;
		public bool is_default;
		public int generation_id;
		public int? evolves_from_species_id = null;
		public int evolution_chain_id;
		public int color_id;
		public int gender_rate;
		public int capture_rate;
		public int base_happiness;
		public bool is_baby;
		public int hatch_counter;
		public int base_hp;
		public int base_attack;
		public int base_defense;
		public int base_sp_attack;
		public List<int> base_effort;
		public int base_sp_defense;
		public int base_speed;
	}

	public class PokemonAbility {
		public int id;
		public List<String> names = new List<String>();
		public List<List<String>> flavor_texts = new List<List<String>>();
		public bool hidden;

		public PokemonAbility Clone(bool h) {
			PokemonAbility a = new PokemonAbility();
			a.id = this.id;
			a.names = this.names;
			a.flavor_texts = this.flavor_texts;
			a.hidden = h;
			return a;
		}
	}

	public class PokemonMove {
		public int type_id;
		public List<String> names = new List<String>();
		public int level;
		public String machine;
		public int? order;
	}

	public class PokemonForm {
		public List<String> names = new List<String>();
		public List<int> types = new List<int>();
		public int id;
		[JsonIgnore]
		public int form_order;

		public static List<int> AuxTypes(int id) {
			List<int> types = new List<int>();
			switch (id) {
				case 493:
					types.Add(1);
					break;
				case 10041:
					types.Add(7);
					break;
				case 10042:
					types.Add(17);
					break;
				case 10043:
					types.Add(16);
					break;
				case 10044:
					types.Add(13);
					break;
				case 10045:
					types.Add(2);
					break;
				case 10046:
					types.Add(10);
					break;
				case 10047:
					types.Add(3);
					break;
				case 10048:
					types.Add(8);
					break;
				case 10049:
					types.Add(12);
					break;
				case 10050:
					types.Add(5);
					break;
				case 10051:
					types.Add(15);
					break;
				case 10052:
					types.Add(4);
					break;
				case 10053:
					types.Add(14);
					break;
				case 10054:
					types.Add(6);
					break;
				case 10055:
					types.Add(9);
					break;
				case 10056:
					types.Add(11);
					break;
				case 10085:
					types.Add(18);
					break;
			}
			if (types.Count != 0) return types;
			return null;
		}
	}

	public class PokemonShape {
		public List<String> names = new List<String>();
		public List<String> awesome_names = new List<String>();
	}

	public class PokedexEntry {
		public List<String> names = new List<String>();
		public int number;
	}

	public class Evolution {
		public int evolved_species_id;
		public int evolution_trigger_id;
		public List<String> evolution_trigger_name = new List<String>();
		public List<String> trigger_item;
		public int? minimum_level;
		public int? gender_id;
		public List<String> location;
		public List<String> held_item;
		public String time_of_day;
		public List<String> known_move;
		public List<String> known_move_type;
		public int? minimum_happiness;
		public int? minimum_beauty;
		public int? minimum_affection;
		public int? relative_physical_stats;
		public List<String> party_species;
		public List<String> party_type;
		public List<String> trade_species;
		public bool needs_overworld_rain;
		public bool turn_upside_down;
	}

	public class EvolutionLink {
		public int id_from;
		public List<String> names_from = new List<String>();
		public Evolution evo;
		public int? id_to;
		public List<String> names_to = new List<String>();
	}
}
