I should write a read me.

Pull requests are welcome if and only if they're on the develop branch. The other branches are hooked into BitBucket pipelines for continuous integration with materialpokedex.com and materialpokedex.com/beta respectively.